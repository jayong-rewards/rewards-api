'use strict';


var mongoose = require('mongoose'),
  Products = mongoose.model('Products');


exports.list_all_products = function(req, res) {
  Products.find({}, function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};



exports.read_a_product = function(req, res) {
  Products.findById(req.params.productId, function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};


exports.update_a_product = function(req, res) {
  Products.findOneAndUpdate({_id: req.params.productId}, req.body, {new: true}, function(err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

