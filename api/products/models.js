'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ProductsSchema = new Schema({
  name: {
    type: String,
    required: 'Product name is required.'
  },
  description: {
    type: String,
    required: 'Description is required.'
  },
  image_url: {
    type: String,
    required: 'Image URL is required.'
  },
  quantity: {
    type: Number,
    required: 'Quantity is required.'
  },
});

module.exports = mongoose.model('Products', ProductsSchema);