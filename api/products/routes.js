'use strict';
module.exports = function(app) {
  var productTask = require('./controllers');

  // Product Routes
  app.route('/products')
    .get(productTask.list_all_products);


  app.route('/product/:productId')
    .get(productTask.read_a_product)
    .patch(productTask.update_a_product);
};