require('dotenv').config();
var express = require('express'),
  cors = require('cors'),
  app = express(),
  port = process.env.PORT || 3500,
  mongoose = require('mongoose'),
  Products = require('./api/products/models'),
  bodyParser = require('body-parser');

const CONNECTION_URI = process.env.CONNECTION_URI || 'mongodb://localhost/test'

console.log(process.env)

mongoose.Promise = global.Promise;
mongoose.connect(CONNECTION_URI);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); 

app.use(cors());

var routes = require('./api/products/routes');
routes(app);

app.listen(port);

console.log('RESTful rewards API server started on: ' + port);